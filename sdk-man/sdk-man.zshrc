#!/bin/zsh

export SDKMAN_DIR="$HOME/.sdkman"

SDKMAN_INIT="$SDKMAN_DIR/bin/sdkman-init.sh"

if [[ -s "$SDKMAN_INIT" ]]; then
  # shellcheck disable=SC1090
  source "$SDKMAN_INIT"
else
  echo "SDK Man! not installed, $SDKMAN_INIT"
fi

