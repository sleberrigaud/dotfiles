#!/usr/bin/env python3

import fnmatch
import os
import subprocess
import sys

HOME = os.path.expanduser('~')
DOTFILES_DIR = os.path.dirname(os.path.realpath(__file__))
COMMANDS=['symlinks', 'setup']

def symlinks(subfolder):
    for f in _find_linkables(subfolder):
        # TODO don't force all the time
        _lt = _link_target(f)
        _lt_dir = os.path.dirname(_lt)
        if not os.path.exists(_lt_dir):
            _run("mkdir", "-p", _lt_dir)
        _run("ln", "-fs", f, _lt)


def setup(subfolder):
    for s in _find_setup(subfolder):
        print(f"Handling: {s}")
        _run("chmod", "755", s)
        _run("sh", "-c", s)


def _find_linkables(subfolder):
    return _find_files('*.symlink', subfolder)


def _link_target(target_file):
    (_, rel_path) = _strunpack(os.path.relpath(target_file, DOTFILES_DIR), separator='/')
    return os.path.join(HOME, '.' + rel_path[:-8]) # also remove the `.symlink`


def _find_setup(subfolder):
    return _find_files('*.setup', subfolder)


def _find_files(pattern, subfolder):
    matches = []
    folder = DOTFILES_DIR if not subfolder else os.path.join(DOTFILES_DIR, subfolder)
    for root, dirnames, filenames in os.walk(folder):
        for filename in fnmatch.filter(filenames, pattern):
            matches.append(os.path.join(root, filename))
    return matches


def _run(*args):
    print(f"Running: {args}")
    subprocess.call(args)


def _strunpack(string, separator):
    _unpacking = string.split(separator, 1)
    return (_unpacking[0], None) if len(_unpacking) == 1 else (_unpacking[0], _unpacking[1])


def main():
    # remove the command itself
    _argv = sys.argv[1:]
    commands = _argv if _argv else []

    for c in commands:
        (command, subfolder) = _strunpack(c, separator=':')
        globals()[command](subfolder)


if __name__ == "__main__":
    main()
