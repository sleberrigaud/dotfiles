#!/bin/zsh

# This will not work if brew is not setup and made available on the PATH _before_ liquidprompt
LIQUID_PROMPT="$(brew --prefix)/share/liquidprompt"

if [[ -s "$LIQUID_PROMPT" ]]; then
  source "$LIQUID_PROMPT"
else
  echo "Liquid Prompt not installed, $LIQUID_PROMPT"
fi
