#!/bin/zsh

alias l='ls -lh'
alias la='l -a'
alias fn='find . -name '

# Enable easy switching between architecture on M1
# https://vineethbharadwaj.medium.com/m1-mac-switching-terminal-between-x86-64-and-arm64-e45f324184d9
alias intel="env /usr/bin/arch -x86_64 /bin/zsh --login"
alias arm="env /usr/bin/arch -arm64 /bin/zsh --login"
