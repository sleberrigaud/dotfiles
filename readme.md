# Inspiration

* [David Gageot's](https://github.com/dgageot/dotfiles)
* [Zach Holman's](https://github.com/holman/dotfiles)

# Notes

## Misc.

### Ordering

Some setups (*.setup) might need to be ordered (don't think so about *.bashrc). For example
Homebrew should come early

### Add an `update` command?

This might be a good idea to update brew, liquidprompt and other things...

## Firefox / Chrome

* Disable saving passwords: https://support.1password.com/disable-browser-password-manager/

## Mac OSX

https://pawelgrzybek.com/change-macos-user-preferences-via-command-line/

* Right (Secondary) click on magic mouse
* Magic mouse tracking speed
* Magic mouse swap between pages

* Speed up tracking for track-pad 

* Configure Internet Accounts
   * Apple Id
   * Google

* Use FiraCode fonts in _many_ places

* Mission Control
    * Dashboard as overlay
    * https://www.apple.com/downloads/dashboard/business/timescroller.html

* Disable System Preferences > Keyboard > Shortcuts > Services > Search Man page index in terminal (conflicts with idea) 
After disabling, the value of `defaults read pbs NSServicesStatus` is:
```
{
    "com.apple.Terminal - Search man Page Index in Terminal - searchManPages" =     {
        "enabled_context_menu" = 0;
        "enabled_services_menu" = 0;
        "presentation_modes" =         {
            ContextMenu = 0;
            ServicesMenu = 0;
        };
    };
}
```
## SDK Man

How to configure correctly the default one to use. `sdk default <sdk> <version>` ... For some reason the setup didn't do it as I wanted/expected...

## IntelliJ IDEA

* Creates a launcher now
* License server https://jetbrains-license-server.internal.atlassian.com/
* Add SDKs
   * python
   * Java (maybe more than one)
   * GraalVM
* Plugins:
   * Bitbucket Linky
   * Sonarlint     
* Config
   * Untick "Reopen last project on startup"
   * Editor > Font
        * Fira Code light
        * Enable ligatures
        * Fallback : Incosolata
   * Appearance & Behavior > Appearance
        * Use custom Font : Fira Code Light 


### Settings

#### KeyMap

* IntelliJ IDEA Classic

## Launchpad

* add to startup items
* remove keep history across restart for clipboard

# Software

## Download and install

* 1Password (+ browser extensions)
* Atom
* Docker
* Dropbox
* IntelliJ IDEA (Ultimate)
* iTerm2
* LaunchBar
* LastPass
* Mailplane
* Moom
* SDK Man!

## From AppStore

* Amphetamine
* WhatsApp Desktop
* Menu Bar Controller for Sonos
* Trello
* Monosnap