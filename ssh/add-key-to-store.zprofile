#!/bin/zsh

# adds the SSH key to the keystore
ssh-add -K ~/.ssh/id_rsa > /dev/null 2>&1
