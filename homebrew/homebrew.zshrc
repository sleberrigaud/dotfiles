#!/bin/zsh

# Add brew binaries _before_ system ones
export PATH="/opt/homebrew/bin:/opt/homebrew/sbin:$PATH"
